FROM node:alpine

RUN apk add --no-cache \
    nss \
    chromium-chromedriver \
    chromium \
    && apk upgrade --no-cache --available

ENV CHROME_BIN /usr/bin/chromium-browser
